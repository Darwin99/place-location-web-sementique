package com.example.demo;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TrainController {

	static Train train1 = new Train("A");
	static Train train2 = new Train("B");
	static String[] lineAStation = { "Ampère-Victor Hugo", "Bellecour", "Charpennes-Charles Hernu", "Cordeliers",
			"Cusset", };
	static String[] lineBStation = { "Brotteaux", "Charpennes-Charles Hernu", "Debourg", };
	static String[] lineCStation = { "croix-Rousse", "Cuire", };
	static String[] lineDStation = { "Bellecour" };
	static String[] lineMStation = { "Ampère-Victor Hugo", "Bellecour", "Brotteaux", "Charpennes-Charles Hernu",
			"Cordeliers", "croix-Rousse", "Cuire", "Cusset", "Debourg", };

	@GetMapping("/stations")
	public String[] getStations() {
//		String[] stations = { "Perrech", "Ampère-Victor Hugo", "Bellecour", "Cordeliers",
//				"Hôtel de Ville-Louis Pradel", "Foch", "Masséna", "Charpennes-Charles Hernu",
//				"République-Villeurbanne", "Gratte-Ciel", "Flachet-Alain Gilles", "Cusset",
//				"Laurent Bonnevay-Astroballe", "Vaulx-en-Velin-La Soie", "Brotteaux","croix-Rousse",
//				"Cuire","Debourg"};
		String[] stations = { "CROIX PAQUET", "CROIX ROUSSE", "CUIRE", "GARE DE PAR DIEUX", "GORGE DE LOUP",
				"GRANDE BLANCHE", "GRATE-CIEL", "JEAN-MACÉ", "MASSÉNA", "PLACHE GUICHARD", "PLACE JEAN JAURÈS",
				"SANS SOUCI", "STADE DE GERLAND", "VALMY" };
		return stations;
	}

	/*
	 * --InitialiseTrain, this function initializes the journeys of the train each
	 * days at the same time
	 */
	public void initialiseTrain() {

		// recovery of stations

		String[] stations = this.getStations();

		/*
		 * The we have to recovery the currrent time in order to make reference of
		 * journeys of the day The date will be also change automatically, but the
		 * journey be gin at 6 am and end the following day at 6 a.m
		 */
		LocalDateTime dateTime = LocalDateTime.now();
		
		ZonedDateTime dateTimed = dateTime.atZone(ZoneId.of("Europe/Paris"));

		// initializing of the start date: the date from which journeys are scheduled
		LocalDateTime startTime = LocalDateTime.of(dateTimed.getYear(), dateTimed.getMonth(), dateTimed.getDayOfMonth(), 6,
				0);
		// The date at which the scheduled journeys for the day will be stopped
		LocalDateTime endTime = startTime.plusDays(1);

		/*
		 * In the aim that, each day, train journeys are generated automatically, we
		 * have to schedule it for each day then after the endTime, the journey will be
		 * scheduled for the current day In this part of program, we will define journey
		 * between each station in double sens, that means that for example, between the
		 * station Perrech and station valmy, there are journey from perrech to valmy
		 * and also from valmy to perrech at the same time
		 */

		TimerTask repeatedTask = new TimerTask() {
			public void run() {
				
				
				/*
				 * This   variables was made for the repeated for the following day oppositely of those which were created previously
				 * with the same name (The role of those is only for initialization)
				 */

				TrainController.train1.setJourneys(new ArrayList<Journey>());
				LocalDateTime dateTime = LocalDateTime.now();
				ZonedDateTime dateTimed = dateTime.atZone(ZoneId.of("Europe/Paris"));
				
				LocalDateTime startTime = LocalDateTime.of(dateTimed.getYear(), dateTimed.getMonth(),
						dateTimed.getDayOfMonth(), 6, 0);
				LocalDateTime endTime = startTime.plusDays(1);

				for (int i = 0; i < stations.length; i++) {

					/*we have to make sure that the following station of the last station is the first station in order to make 
					*cycle list
					*/
					if (i != (stations.length - 1)) {
						//creation of journey between the station at index i with all the rest of stations
						for (int j = i + 1; j < stations.length; j++) {

							LocalDateTime currentTime = startTime.plusSeconds(0);
							
							// here, we make journey in double senses between station at index i and station at index j during the day until the endTime 
							//which it at 6 a.m
							
							while (currentTime.isBefore(endTime)) {
								//the duration between two consecutive journeys is of five minutes 
								LocalDateTime previousTime = currentTime.plusMinutes(1);
								currentTime = previousTime.plusMinutes(5);

								//definition of journey in the first sens
								Journey currentJourney = new Journey(stations[i], stations[j], previousTime,
										currentTime);
								
								//addition of the first jhourney to the train
								TrainController.train1.addJourney(currentJourney);
								
								//defenition of the second sens of the journey
								Journey currentJourney2 = new Journey(stations[j], stations[i], previousTime,
										currentTime);
								//addition of the second journey to the train
								TrainController.train1.addJourney(currentJourney2);

							}

						}
					} else {
						
						//this is the same like previously but for the first stations and the last
						LocalDateTime currentTime = startTime.plusSeconds(0);
						while (currentTime.isBefore(endTime)) {

							LocalDateTime previousTime = currentTime.plusMinutes(1);
							currentTime = previousTime.plusMinutes(5);

							Journey currentJourney = new Journey(stations[i], stations[0], previousTime, currentTime);
							TrainController.train1.addJourney(currentJourney);

							Journey currentJourney2 = new Journey(stations[0], stations[i], previousTime, currentTime);
							TrainController.train1.addJourney(currentJourney2);

						}

					}

				}

			}
		};

		Timer timer = new Timer("Timer");
		Date now = new Date();

		// the period between the scheduling of tasks have to be of one day in long integer which are millis seconds
//	    long period = 2555000;
		
		//this duration is the defference between the end time and the start time
		
		long period = endTime.toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli()
				- startTime.toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli();
		System.out.println("Period*************************:   " + period);
		timer.scheduleAtFixedRate(repeatedTask, now, period);

	}

	@GetMapping("/journey/{depatureStation}/{arrivalStation}")
	public ArrayList<Journey> getJourneyWithArrivalStations(@PathVariable String depatureStation,
			@PathVariable String arrivalStation) {
		
		/*
		 * This function give the journeys after now from the departure  station to the arrival station
		 */

		ArrayList<Journey> comingJourneys = new ArrayList<>();
		comingJourneys = TrainController.train1.getCommingBus(depatureStation, arrivalStation);
		// here, for each journey, we have to make the delay
		ArrayList<Journey> comingJourneyWithDalay = new ArrayList<>();
		
		
		//after recovery that, we have to define the delay to wait the the ram come to the asking  stations
		//but at the first time, the dalay attribute is for define the delay that the firme of trains could have because of somme causes
		//and this delay that we compute could be compute by the user service

		for (Journey journey : comingJourneys) {

			//the duration id from now up to the arrival hour
			
			LocalDateTime now = LocalDateTime.now();
			long duration = now.until(journey.getArrivalHour(), ChronoUnit.MINUTES);
			/*
			 * here we have to customize by making controls: if the duration is greater than
			 * 60 for seconds, we take minutes, else if it is greater than 60 minutes, we
			 * use hours...
			 */
			journey.setDelay(duration);

			comingJourneyWithDalay.add(journey);
		}

		return comingJourneyWithDalay;
	}

	@GetMapping("/journey/{station}")
	public ArrayList<Journey> getJourney(@PathVariable String station) {
		
		/*
		 * This function give the journeys in which station is the departure station
		 */

		ArrayList<Journey> comingJourneys = new ArrayList<>();
		comingJourneys = TrainController.train1.getCommingBus(station);
		ArrayList<Journey> comingJourneyWithDalay = new ArrayList<>();

		for (Journey journey : comingJourneys) {

			//the duration id from now up to the arrival hour
			
			LocalDateTime now = LocalDateTime.now();
			long duration = now.until(journey.getArrivalHour(), ChronoUnit.MINUTES);
			/*
			 * here we have to customize by making controls: if the duration is greater than
			 * 60 for seconds, we take minutes, else if it is greater than 60 minutes, we
			 * use hours...
			 */
			journey.setDelay(duration);

			comingJourneyWithDalay.add(journey);
		}

		return comingJourneyWithDalay;
	
	}


}
