package com.example.demo;

import java.time.LocalDateTime;

public class Journey {

	private String startStation;
	private String arrivalStation;
	private LocalDateTime departureHour;
	private LocalDateTime arrivalHour;
	private long delay;

	public Journey( String startStation,String arrivalStation, LocalDateTime departureHour, LocalDateTime arrivalHour) {
		super();
		this.arrivalStation = arrivalStation;
		this.startStation = startStation;
		this.departureHour = departureHour;
		this.arrivalHour = arrivalHour;
		this.delay = 0;
	}

	public Journey() {

	}

	public String getArrivalStation() {
		return arrivalStation;
	}

	public void setArrivalStation(String arrivalStation) {
		this.arrivalStation = arrivalStation;
	}

	public String getStartStation() {
		return startStation;
	}

	public void setStartStation(String startStation) {
		this.startStation = startStation;
	}

	public LocalDateTime getDepartureHour() {
		return departureHour;
	}

	public void setDepartureHour(LocalDateTime departureHour) {
		this.departureHour = departureHour;
	}

	public LocalDateTime getArrivalHour() {
		return arrivalHour;
	}

	public void setArrivalHour(LocalDateTime arrivalHour) {
		this.arrivalHour = arrivalHour;
	}
	
	public void setDelay(long delay) {
		this.delay = delay;
	}
	
	public long getDelay() {
		return delay;
	}

	

	

}
