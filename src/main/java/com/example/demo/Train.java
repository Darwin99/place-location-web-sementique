package com.example.demo;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.catalina.util.ConcurrentDateFormat;

public class Train {

	private ArrayList<Journey> journeys;

	private String name;

	public Train(ArrayList<Journey> journeys) {
		super();
		this.journeys = journeys;
	}

	public Train() {
		super();
		this.journeys = new ArrayList<>();
	}

	public Train(String name) {

		super();
		this.name = name;
		this.journeys = new ArrayList<>();

	}

	public ArrayList<Journey> getJourneys() {
		return journeys;
	}

	public void setJourneys(ArrayList<Journey> journeys) {
		this.journeys = journeys;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Journey> getCommingBus(String station) {
		
		/*
		 * This function return the journeys in which the parameter station is the departure station
		 */
		Date date = new Date();
		

		ArrayList<Journey> stationJourneys = new ArrayList<>();
		this.journeys.stream().filter(j -> ((j.getStartStation().equalsIgnoreCase(station)))).forEach(stationJourneys::add);
		ArrayList<Journey> t = new ArrayList<Journey>();

		
		//there, we filter which one that comes after now

		for (Journey journey : stationJourneys) {

			if (journey.getDepartureHour().isAfter(LocalDateTime.now())
					|| journey.getDepartureHour().isEqual(LocalDateTime.now())) {

				t.add(journey);

			}

		}

		return t;
	}

	public ArrayList<Journey> getCommingBus(String departureStation, String arrivalStation) {
			Date date = new Date();
			// for each journey, we recover the journey of that is coming

			ArrayList<Journey> stationJourneys = new ArrayList<>();
			this.journeys.stream().filter(j -> ((j.getStartStation().equalsIgnoreCase(departureStation)))).forEach(stationJourneys::add);
			ArrayList<Journey> stationsForArrival = new ArrayList<Journey>();
			
			stationJourneys.stream().filter(j -> ((j.getArrivalStation().equalsIgnoreCase(arrivalStation)))).forEach(stationsForArrival::add);
			
			ArrayList<Journey> t = new ArrayList<Journey>();


			for (Journey journey : stationsForArrival) {

				if (journey.getDepartureHour().isAfter(LocalDateTime.now())
						|| journey.getDepartureHour().isEqual(LocalDateTime.now())) {

					t.add(journey);

				}

			}

			return t;
		}



	
	
	public boolean addJourney(Journey journey) {
		return this.journeys.add(journey);
	}

}
